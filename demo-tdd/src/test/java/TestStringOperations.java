import org.junit.Test;

import junit.framework.Assert;
import junit.framework.TestCase;

public class TestStringOperations extends TestCase {

	
	@Test
	public void testOperations() {
		String expMessage = "Hello, world";
		String message2 = new String("Hello, world");
		
//		expMessage = null;
		
		Assert.assertTrue(expMessage.length() > 0);
		
		Assert.assertEquals(expMessage, message2);
		
		Assert.assertNotSame(expMessage, message2);
	}
	
}
