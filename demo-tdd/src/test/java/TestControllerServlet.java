import static org.junit.Assert.*;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import com.capgemini.ControllerServlet;
import com.capgemini.dao.CarDAO;
import com.capgemini.dto.CarDTO;

public class TestControllerServlet {

	@Test
	@Ignore
	public void test() {
		fail("Not yet implemented");
	}

	@Test
	public void testViewAllCars() throws ServletException, IOException {
//		SETUP
		
		CarDAO mockCarDAO = Mockito.mock(CarDAO.class);
		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
		
//		TEST FIXTURE
		List<CarDTO> cars = new LinkedList<CarDTO>();
		CarDTO car = new CarDTO();
		car.setId(1);
		car.setMake("Honda");
		car.setModel("City");
		car.setModelYear("2000");
		
		
		cars.add(car);
		
		
		Mockito.when(mockCarDAO.findAll()).thenReturn(cars);
		
		
//		EXECUTION
		
		
		ControllerServlet myServlet = new ControllerServlet();
		myServlet.setCarDao(mockCarDAO);
		myServlet.doGet(request, response);
		
		
//		VERIFICATION

		Mockito.verify(mockCarDAO).findAll();
		
		
//		TEARDOWN
		
		
		
		
		
	}
	
	
	
	
	
	
}
