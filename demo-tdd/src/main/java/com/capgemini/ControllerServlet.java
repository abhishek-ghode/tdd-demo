package com.capgemini;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.capgemini.dao.CarDAO;
import com.capgemini.dto.CarDTO;

public class ControllerServlet extends HttpServlet{
	
	private CarDAO carDao;

	public void setCarDao(CarDAO carDao) {
		this.carDao = carDao;
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		TODO: read action parameter from given http request
			
		
		
		//		TODO: find all cars from persistence
				List<CarDTO> cars = carDao.findAll();
		
		//		TODO: store car details in request scope
				request.setAttribute("carList", cars);
				System.out.println(cars);
				
		//		TODO: set destination page
				String destinationPage = "carList.jsp";
	}
	
	
	
}
